from django.apps import AppConfig


class AuthConfig(AppConfig):
    name = label = 'idea_party.auth'
